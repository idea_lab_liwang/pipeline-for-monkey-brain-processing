# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary: Pipeline for monkey brain processing 
* Version: 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up: Caffe + Python
Input: NIfTI files and ages in month

Output: skull striped images, N4 corrected images, tissue labeled images, parcellation images

Functions: 
AC-PC based alignment -> skull stripping -> N4 correction -> tissue segmentation -> parcellation -> longitudinal segmentation/parcellation if later time-point is older than 06 months 

Example: Monkey_brain_processing.sh MONKEY110215-61-months_A53T.nii 61m 

-a: default 1: AC-PC based alignment; 0: skip AC-PC based alignment
-s: default 1: skull stripping; 0: skip skull stripping
-n: default 1: N4 correction; 0: skip N4 correction
-seg: default 1: tissue segmentation; 0: skip tissue segmentation
-p: default 1: parcellation; 0: skip parcellation (using predefined atlas)


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact